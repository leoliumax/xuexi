var teststr = "autoprefixer browser-sync crypto-md5 del gulp gulp-cssnano gulp-ejs gulp-ftp gulp-if gulp-imagemin gulp-lazyimagecss gulp-less gulp-postcss gulp-posthtml gulp-rename gulp-replace gulp-rev-all gulp-rev-delete-original gulp-tmtsprite gulp-uglify gulp-usemin2 gulp-util gulp-webp gulp-zip imagemin-pngquant lodash postcss-pxtorem posthtml-px2rem quick-local-ip rc rd tmt-ejs-helper browserify cssnano gulp-add-src gulp-autoprefixer gulp-cache gulp-cached gulp-clean gulp-concat gulp-file-include gulp-filter gulp-html-extend gulp-html-replace gulp-jshint gulp-markdown gulp-minify-css gulp-minify-html gulp-notify gulp-rev gulp-rev-append gulp-rev-replace gulp-sass gulp-template gulp-webserver jshint run-sequence underscore vinyl-buffer vinyl-paths vinyl-source-stream yargs";

function uniqarr(arr, nosort, exp, unobj) {
	/*
	 *数组去重函数
	 *parm: 参数arr为要处理的数组，默认以最优的速度处理字符串数组，参数 [nosort] 为true时不对数组重新排序,
	 *当想处理包含对象的数组时使用,请传入exp参数：默认不将对象不会去重,会保留所有对象,如果想对象也去重,传入参数[unobj]，
	 */
	var retarr = [],
		_arr = nosort ? arr.sort() : arr,
		hash = {},
		_exp = exp && true,
		_unobj = unobj && true;
	cacheobj = [],
		str = Object.prototype.toString;
	if (!_exp) {
		for (var i = 0, max = _arr.length; i < max; i++) {
			var val = _arr[i];
			if (!hash[val]) {
				retarr.push(val);
				hash[val] = true;
			}
		}
		return retarr;
	}
	var unobj = function(obja, objb) {
		var a = obja,
			b = objb;
		console.log(a.length)
		if (!((typeof a === typeof b) && (typeof a.length === typeof b.length) && (a.length === b.length))) {
			return false;
		}
		for (var n in a) {
			var vala = a[n],
				valb = b[n];
			if (istype(vala)) {
				if (!unobj(vala, valb)) {
					return false;
				}
			} else if (vala !== valb) {
				return false;
			}
		}
		return true;
	}

	function uniqobj(vobj) {
		var _obj = vobj;
		if (!_unobj) return false;
		for (var i in cacheobj) {
			var has = unobj(_obj, cacheobj[i]);
			if (has) {
				return true;
			}
		}
		return false;
	}

	function istype(val) {
		var type = str.call(val);
		return _exp && (type === "[object Array]") || (type === "[object Object]");
	}
	for (var i = 0, max = _arr.length; i < max; i++) {
		var val = _arr[i];
		if (!istype(val)) {
			if (!hash[val]) {
				retarr.push(val);
				hash[val] = val;
			}
		} else {
			var che = uniqobj(val);
			if (!che) {
				retarr.push(val);
				cacheobj.push(val);
			}
		}
	}
	return retarr;
}
var ars = ["stlkj", "lkjslfkja", "lkjasdflj"];

//如果已经对传入的数组进行排序，给参数sort传入true，想在函数内执行排序传递关键字'sort'，注意：这种方式会影响传入的原数组arr也进行排序。所以建议在传入之前进行排序会好些。当然也可以用深复制在函数内进行深复制后再排序，但这样会影响性能，请根据情况自行选择使用；否则传递false;
//传入参数unobj时可以处理带对象的数组比较；
function uniq(arr, sort, unobj) {
	var _arr = arr,
		result = [],
		type,
		_sort = sort && (sort == "sort" ? _arr.sort() : true), //用sort()对数组进行排序
		_unobj = unobj && true,
		prev = [],
		hash = {};
	console.log(_arr, sort, "\n", "arr", arr)
	for (var i = 0, max = _arr.length; i < max; i++) {
		var val = _arr[i];
		if (_sort) {
			if (prev !== val) result.push(val);
			prev = val;
		} else if (unobj) {
			type = typeof val === 'object';
			if (type) {
				val = JSON.stringify(val);
			}
			if (!hash[val]) {
				result.push(_arr[i]);
				hash[val] = true;
			}
		} else {
			if (!hash[val]) {
				result.push(val);
				hash[val] = true;
			}
		}
	}
	return result;
}
//返回字符串中重复最多的字符和次数；
function Clone(obj, deep) {
	var arrtype = "[object Array]",
		istype = function(o) {
			return Object.prototype.toString.call(o) === arrtype;
		},
		result = deep || {};
	for (var i in obj) {
		if ((typeof obj[i] === 'object') && (obj[i] !== null)) {
			console.log(istype(obj[i]), obj[i]);
			result[i] = istype(obj[i]) ? [] : {};
			Clone(obj[i], result[i]);
		} else {
			result[i] = obj[i];
		}
	}

	return result;
}


function strmaxre(str) {
	console.time('end');
	var arr = str.split(''),
		_str, num = 0,
		maxnum = 0,
		maxval, strname = [],
		hash = {},
		result;
	_str = arr.sort();
	for (var i = 0, l = _str.length; i < l; i++) {
		var val = _str[i];
		if (!hash[val]) {
			var arr = str.split(''),
				_str, num = 0,
				maxnum = 0,
				strname = [],
				hash = {},
				result;
			_str = arr.sort();
			//		console.log(_str.length)
			for (var i = 0, l = _str.length; i < l; i++) {
				var val = _str[i],
					nextval = _str[i + 1];
				if (!hash[val]) {
					hash[val] = true;
					num = 1;
				} else {
					num++;
					if (maxnum < num) {
						strname = [val];
						maxnum = num;
					} else if (maxnum === num) {
						if (nextval !== val && maxnum < num) { //加了对下一个字符是否一样，也就是同一个字符的统计是否结束的判断，统计结束才对数量进行比较，避免相等时也会作比较。
							strname = [val];
							maxnum = num;
							//				console.log('<<<<',val,num);
						} else if (maxnum === num) {
							strname.indexOf(val) === (-1) && strname.push(val);
							//				console.log('===',val,num);
						}
					}
				}
				result = strname + '重复最多，共' + maxnum + '次';
				//	result = {
				//		str:[strname].toString(),
				//		num:maxnum
				//	}
				result = strname + '重复最多，共' + maxnum + '次';
				console.timeEnd('end');
				return result;
			}
			strmaxre("cccaaaabbbbccdddddd");
			//字符串反排函数,字符长度计算，字符统计。中文算2个字符；
			var str = "abcdABCD就烟云过眼";

			function re(str) {
				var s = str,
					args = [],
					len = 0;
				//	console.log(s);
				//	for (var i = 0, length = s.length; i < length; i++) {
				//		args.push(s.charAt(i));
				//	}
				//	args.reverse();
				//	return args.join('');
				return str.reverse().join('');
			}
		}
	}
}

function mix(target, source) { //如果最后参数是布尔，判定是否覆写同名属性
	var args = [].slice.call(arguments),
		i = 1,
		key,
		ride = typeof args[args.length - 1] == "boolean" ? args.pop() : true;
	if (args.length === 1) { //处理$.mix(hash)的情形
		target = !this.window ? this : {};
		i = 0;
	}
	while ((source = args[i++])) {
		for (key in source) { //允许对象糅杂，用户保证都是对象
			if (ride || !(key in target)) {
				target[key] = source[key];
			}
		}
	}
	return target;
}

function test() {
	for (var i = 0, max = 10; i < max; i++) {
		var s = i;
		var n = s++;
	}
	console.log("warp", s, i, max)
}

Function.prototype.bind = function(oThis) {
	if (typeof this !== 'function') {
		throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
	}
	var fSlice = Array.prototype.slice,
		aArgs = fSlice.call(arguments, 1),
		fToBind = this,
		fNOP = function() {},
		fBound = function() {
			return fToBind.apply(this instanceof fNOP ? this : oThis || window, aArgs.concat(fSlice.call(arguments)));
		};
	if (this instanceof fNOP) {
		alert()
	};
	fNOP.prototype = this.prototype;
	fBound.prototype = new fNOP();
	return fBound;
};

var CookieFn = {
	deSerial: function(serial) {//返回串行化的对象
		var pent = serial.split('&'),
			result = {};
		for (var i = 0, max = pent.length; i < max; i++) {
			var sub = pent[i].split("=");
			result[sub[0]] = sub[1];
		}
		return result;
	},
	enSerial: function(obj) {//串行化一个JSON对象
		var result = [];
		var per;
		for (i in obj) {
			if (i.length > 0 && obj.hasOwnProperty(i)) {
				per = i + '=' + obj[i];
				result.push(per);
			}
		}
		return result.join("&");
	},
	get: function(name, subName) {
		var subcookie = this.getAll(name);
		return subcookie ? subcookie[subName] : null;
	},
	getAll: function(name) {
		var cookieName = encodeURIComponent(name) + '=',
			startIndex = document.cookie.indexOf(cookieName),
			cookieValue=null;
		if (startIndex > -1) {
			var endIndex = document.cookie.indexOf(';', startIndex);
		}
		if (endIndex == -1) {
			endIndex = document.cookie.length;
		}
		cookieValue = decodeURIComponent(document.cookie.substring(startIndex + cookieName.length, endIndex));
		if(cookieValue.indexOf("&") > -1){
			return this.deSerial(cookieValue);
		}
		return cookieValue;
	},
	set: function(_opt) { //opt: name, subname,subvalue, expires, path, domain, secure;
		var opt=_opt,
			subcookie = this.getAll(opt.name)||{};//如果之前不是序列化的，转换成序列化状态;
			console.log(subcookie);
		if (subcookie){
			if (typeof subcookie == 'object'){
				subcookie[opt.subname] = opt.subvalue;
			}else{
				subcookie = {[opt.name]:subcookie};
				subcookie[opt.subname] = opt.subvalue;
			}
			opt.value = subcookie;
			this.setAll(opt);
		}
		
	},
	setAll: function(_opt) { // opt: name, value（支持能序列化的JSON对象）, expires, path, domain, secure;
		var opt=_opt,
			hasserial = opt.value && (typeof opt.value === 'object'),//判断是否要进行序列化；
			cookieData = encodeURIComponent(opt.name) + '=' + encodeURIComponent(!hasserial ? opt.value : this.enSerial(opt.value));
		if (opt.expires instanceof Date) {
			cookieData += opt.expires.toGMTString();
		} else if (opt.expires) {
			var __date = new Date();
			__date.setTime(__date.getTime() + (opt.expires * 24 * 60 * 1000));
			cookieData += "; expires=" + __date.toGMTString();
		}
		if (opt.path) {
			cookieData += "; path=" + opt.path;
		}
		if (opt.domain) {
			cookieData += "; doamin=" + opt.doamin;
		}
		if (opt.path) {
			cookieData += "; secure=" + opt.secure;
		}
		document.cookie = cookieData;
	},
	unset: function(name) {
		this.set({
			'name': name,
			'value': ''
		});
	}
}


//串行化一个JSON对象
var enSerial = function(obj) {
	var result = [];
	var per;
	for (i in obj) {
		if (i.length > 0 && obj.hasOwnProperty(i)) {
			per = i + '=' + obj[i];
			result.push(per);
		}
	}
	return result.join("&");
}
var deSerial = function(serial) {
	var pent = serial.split('&'),
		result = {};
	for (var i = 0, max = pent.length; i < max; i++) {
		var sub = pent[i].split("=");
		result[sub[0]] = sub[1];
	}
	return result;
}