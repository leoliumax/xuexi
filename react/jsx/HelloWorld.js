var Comment = React.createClass({
	getDefaultProps: function() {
		return {
			test: 'woshi'
		};
	},
	rawMarkup: function() {
		var rawMarkup = marked(this.props.children.toString(), {
			sanitize: true
		});
		return {
			__html: rawMarkup
		};
	},
	render: function() {
		return ( < div className = "Comment" >
			< h2 className = "CommentAuthor" > {
				this.props.author
			} < /h2> < span dangerouslySetInnerHTML = {
			this.rawMarkup()
		}
		/> < /div > );
}
});
var CommentList = React.createClass({
	render: function() {
		var commentNodes = this.props.data.map(function(comment) {
				return ( < Comment author = {
						comment.author
					} > {
						comment.text
					} < /Comment>);
				});
			return ( < div className = "commentList" > {
					commentNodes
				} < /div>);
			}
		});

	var CommentForm = React.createClass({
		handleSubmit: function(e) {
			e.preventDefault();
			var author = this.refs.author.getDOMNode().value.trim();
			var text = this.refs.text.getDOMNode().value.trim();
			if (!text || !author) {
				return;
			}
			this.props.onCommentSubmit({
				'author': author,
				'text': text
			});
			this.refs.author.value = '';
			this.refs.text.value = '';
			return;
		},
		render: function() {
			return ( < form className = "commentForm"
				onSubmit = {
					this.handleSubmit
				} >
				< span style = {
					{
						display: 'block',
						color: 'blue'
					}
				} > Hello, This a React commentForm.Ele < /span>    				 < input type = "text"
				ref = "author"
				placeholder = "Your name" / >
				< input type = "text"
				ref = "text"
				placeholder = "Say something……" / >
				< input type = "submit"
				value = "Post Input" / >
				< /form>
			);
		}
	});
	var CommentBox = React.createClass({
			getInitialState: function() {
				return {
					data: []
				};
			},
			loadCommentsFromServer: function() {
				$.ajax({
					url: this.props.url,
					dataType: 'json',
					cache: false,
					success: function(data) {
						this.setState({
							'data': data
						});
					}.bind(this),
					error: function(xhr, status, err) {
						console.error(this.props.url, status, err.toString());
					}.bind(this)
				});
			},
			handleCommentSubmit: function(comment) {
				$.ajax({
					url: this.props.url,
					dataType: 'json',
					type: 'POST',
					data: comment,
					success: function(data) {
						this.setState({
							'data': data
						});
					}.bind(this),
					error: function(xhr, status, err) {
						console.error(this.props.url, status, err.toString());
					}.bind(this)
				});
			},
			componentDidMount: function() {
				this.loadCommentsFromServer();
				setInterval(this.loadCommentsFromServer, this.props.pollInterval);
			},
			render: function() {
				return ( < div className = "commentBox" >
					< h1 > Comments < /h1> < CommentList data = {
					this.state.data
				}
				/> < CommentForm onCommentSubmit = {
				this.handleCommentSubmit
			}
			/> < /div >
		);
	}
});

React.render( < CommentBox url = "http://127.0.0.1:3000/api/comments"
	pollInterval = {
		2000
	}
	/>,
	document.getElementById('content')
);