var gulp = require('gulp'),
	md = require('gulp-markdown'),
	extender = require('gulp-html-extend'),
	webserver = require('gulp-webserver');

gulp.task('md', function() {
	return gulp.src('./md/**/*.md')
		.pipe(md())
		.pipe(gulp.dest('./md/md2html/'));
});

gulp.task('extend', ['md'], function() {
	return gulp.src('./md/md2html/**/*.html')
		.pipe(extender({
			annotations: true,
			verbose: false
		}))
		.pipe(gulp.dest('./dist/'));
});

gulp.task('watch', function() {
	gulp.watch('./md/**/*.md', ['extend']);
});

gulp.task('webserver', ['extend'], function() {
	return gulp.src('./dist/')
		.pipe(webserver({
			port: 1111,
			livereload: true,
			directoryListing: false,
			open: true,
			fallback: 'test.html'
		}));
});

gulp.task('default', ['watch', 'webserver']);