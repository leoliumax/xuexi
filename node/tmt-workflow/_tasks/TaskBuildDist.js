var _ = require('lodash');
var fs = require('fs');
var del = require('del');
var path = require('path');
var ejs = require('gulp-ejs');
var gulpif = require('gulp-if');
var less = require('gulp-less');
var util = require('./lib/util');
var uglify = require('gulp-uglify');
var usemin = require('gulp-usemin2');
var lazyImageCSS = require('gulp-lazyimagecss'); // 自动为图片样式添加 宽/高/background-size 属性
var minifyCSS = require('gulp-cssnano');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var autoprefixer = require('gulp-autoprefixer'); // 添加 CSS 浏览器前缀
var tmtsprite = require('gulp-tmtsprite'); // 雪碧图合并
var ejshelper = require('tmt-ejs-helper');
var postcss = require('gulp-postcss'); // CSS 预处理
var postcssPxtorem = require('postcss-pxtorem'); // 转换 px 为 rem
var postcssAutoprefixer = require('autoprefixer');
var posthtml = require('gulp-posthtml');
var posthtmlPx2rem = require('posthtml-px2rem');
var RevAll = require('gulp-rev-all'); // reversion
var revDel = require('gulp-rev-delete-original');
var changed = require('./common/changed')();
var filter = require('gulp-filter');
var rename = require('gulp-rename');

var paths = {};

module.exports = function(gulp, cfg) {
	var config = cfg;

	function initPaths() {
		paths = config.DistPaths;
	}

	if (config.DistPaths) {
		initPaths();
	}
	var webp = require('./common/webp')(config);

	var lazyDir = config.lazyDir || ['../slice'];

	var postcssOption = [];

	if (config.supportREM) {
		postcssOption = [
			postcssAutoprefixer({
				browsers: ['last 5 versions']
			}),
			postcssPxtorem({
				root_value: '20', // 基准值 html{ font-zise: 20px; }
				prop_white_list: [], // 对所有 px 值生效
				minPixelValue: 2 // 忽略 1px 值
			})
		]
	} else {
		postcssOption = [
			postcssAutoprefixer({
				browsers: ['last 5 versions']
			})
		]
	}

	// 清除 dist 目录
	function delDist() {
		return del([paths.dist.dir]);
	}

	// 清除 tmp 目录
	function delTmp() {
		return del([paths.tmp.dir]);
	}

	//编译 less
	function compileLess() {
		return gulp.src(paths.src.less)
			.pipe(less())
			.pipe(autoprefixer('last 5 version'))
			.pipe(lazyImageCSS({
				imagePath: lazyDir
			}))
			.pipe(tmtsprite({
				margin: 4
			}))
			.pipe(gulpif('*.png', gulp.dest(paths.tmp.sprite), gulp.dest(paths.tmp.css)));
	}

	//自动补全
	function compileAutoprefixer() {
		return gulp.src('./tmp/css/style-*.css')
			.pipe(postcss(postcssOption))
			.pipe(gulp.dest('./tmp/css/'));
	}

	//CSS 压缩
	function miniCSS() {
		return gulp.src('./tmp/css/style-*.css')
			.pipe(minifyCSS({
				safe: true,
				reduceTransforms: false,
				advanced: false,
				compatibility: 'ie7',
				keepSpecialComments: 0
			}))
			.pipe(gulp.dest('./tmp/css/'));
	}

	//图片压缩
	function imageminImg() {
		return gulp.src(paths.src.img)
			.pipe(imagemin({
				use: [pngquant()]
			}))
			.pipe(gulp.dest(paths.tmp.img));
	}

	//复制媒体文件
	function copyMedia() {
		return gulp.src(paths.src.media, {
			base: paths.src.dir
		}).pipe(gulp.dest(paths.dist.dir));
	}

	//JS 压缩
	function uglifyJs() {
		return gulp.src(paths.src.js, {
				base: paths.src.dir
			})
			.pipe(uglify())
			.pipe(gulp.dest(paths.tmp.dir));
	}

	function uglifylibJs() {
		return gulp.src(paths.src.lib, {
				base: paths.src.dir
			})
			.pipe(gulp.dest(paths.tmp.dir))
			.pipe(filter(['*', '!*.min.js'])) // 筛选出管道中的非 *.min.js 文件
			// .pipe(jshint('.jshintrc')) // js的校验与合并，根据需要开启
			// .pipe(jshint.reporter('default'))
			// .pipe(concat('main.js'))
			// .pipe(gulp.dest('dist/js'))
			.pipe(rename({
				suffix: '.min'
			}))
			.pipe(uglify())
			.pipe(gulp.dest(paths.tmp.dir));
	}

	function minilibCSS() {
		return gulp.src(paths.src.lib, {
				base: paths.src.dir
			})
			.pipe(filter(['*', '!*.min.css']))
			.pipe(postcss(postcssOption))
			.pipe(rename({
				suffix: '.min'
			}))
			.pipe(minifyCSS({
				safe: true,
				reduceTransforms: false,
				advanced: false,
				compatibility: 'ie7',
				keepSpecialComments: 0
			}))
			.pipe(gulp.dest(paths.src.dir));
	}
	//雪碧图压缩
	function imageminSprite() {
		return gulp.src('./tmp/sprite/**/*')
			.pipe(imagemin({
				use: [pngquant()]
			}))
			.pipe(gulp.dest(paths.tmp.sprite));
	}

	//html 编译
	function compileHtml() {
		return gulp.src(paths.src.html)
			.pipe(ejs(ejshelper()))
			.pipe(gulpif(
				config.supportREM,
				posthtml(
					posthtmlPx2rem({
						rootValue: 20,
						minPixelValue: 2
					})
				)))
			.pipe(usemin({ //JS 合并压缩
				jsmin: uglify()
			}))
			.pipe(gulp.dest(paths.tmp.html));
	}

	//webp 编译
	function supportWebp() {
		if (config['supportWebp']) {
			return webp();
		} else {
			return function noWebp(cb) {
				cb();
			}
		}
	}

	//新文件名(md5)
	function reversion(cb) {
		var revAll = new RevAll({
			fileNameManifest: 'manifest.json',
			dontRenameFile: ['.html', '.php']
		});

		if (config['reversion']) {
			return gulp.src(['./tmp/**/*'])
				.pipe(revAll.revision())
				.pipe(gulp.dest(paths.tmp.dir))
				.pipe(revDel({
					exclude: /(.html|.htm)$/
				}))
				.pipe(revAll.manifestFile())
				.pipe(gulp.dest(paths.tmp.dir));
		} else {
			cb();
		}
	}

	function findChanged(cb) {

		if (!config['supportChanged']) {
			return gulp.src('./tmp/**/*', {
					base: paths.tmp.dir
				})
				.pipe(gulp.dest(paths.dist.dir))
				.on('end', function() {
					delTmp();
				})
		} else {
			var diff = changed('./tmp');
			var tmpSrc = [];

			if (!_.isEmpty(diff)) {

				//如果有reversion
				if (config['reversion'] && config['reversion']['available']) {
					var keys = _.keys(diff);

					//先取得 reversion 生成的manifest.json
					var reversionManifest = require(path.resolve('./tmp/manifest.json'));

					if (reversionManifest) {
						reversionManifest = _.invert(reversionManifest);

						reversionManifest = _.pick(reversionManifest, keys);

						reversionManifest = _.invert(reversionManifest);

						_.forEach(reversionManifest, function(item, index) {
							tmpSrc.push('./tmp/' + item);
							console.log('[changed:] ' + util.colors.blue(index));
						});

						//将新的 manifest.json 保存
						fs.writeFileSync('./tmp/manifest.json', JSON.stringify(reversionManifest));

						tmpSrc.push('./tmp/manifest.json');
					}
				} else {
					_.forEach(diff, function(item, index) {
						tmpSrc.push('./tmp/' + index);
						console.log('[changed:] ' + util.colors.blue(index));
					});
				}

				return gulp.src(tmpSrc, {
						base: paths.tmp.dir
					})
					.pipe(gulp.dest(paths.dist.dir))
					.on('end', function() {
						delTmp();
					})

			} else {
				console.log('Nothing changed!');
				delTmp();
				cb();
			}
		}

	}

	//加载插件
	function loadPlugin(cb) {
		util.loadPlugin('build_dist');
		cb();
	}

	//注册 build_dist 任务
	gulp.task('build_dist', gulp.series(
		delDist,
		compileLess,
		compileAutoprefixer,
		miniCSS,
		gulp.parallel(
			imageminImg,
			imageminSprite,
			copyMedia,
			uglifyJs
		),
		uglifylibJs,
		minilibCSS,
		compileHtml,
		reversion,
		supportWebp(),
		findChanged,
		loadPlugin
	));
};