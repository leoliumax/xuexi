var config = {
	'projectName': 'www',
	"ftp": {
		"host": "xx.xx.xx.xx",
		"port": "8021",
		"user": "tmt",
		"pass": "password",
		"remotePath": "remotePath",
		"includeHtml": true //ftp 上传是否包含 html
	},

	"livereload": {
		"available": true, //开启自动刷新j 
		"port": 8080,
		"startPath": "html/TmTIndex.html"
	},

	//路径相对于 tasks/plugins 目录
	"plugins": {
		"build_devAfter": ["TmTIndex"],
		"build_distAfter": [],
		"ftpAfter": ["ftp"]
	},
	"DevPaths": {
		src: {
			dir: './src',
			img: './src/img/**/*.{JPG,jpg,png,gif}',
			slice: './src/slice/**/*.png',
			js: './src/js/**/*.js',
			lib: './src/lib/**/*',
			media: './src/media/**/*',
			less: './src/css/style-*.less',
			lessAll: './src/css/**/*.less',
			html: ['./src/html/**/*.html', '!./src/html/_*/**.html', '!./src/html/_*/**/**.html'],
			htmlAll: './src/html/**/*.html'
		},
		dev: {
			dir: './dev',
			css: './dev/css',
			html: './dev/html'
		}
	},
	"DistPaths": {
		src: {
			dir: './src',
			img: './src/img/**/*.{JPG,jpg,png,gif}',
			slice: './src/slice/**/*.png',
			js: './src/js/**/*.js',
			lib: './src/js/**/*',
			media: './src/media/**/*',
			less: './src/css/style-*.less',
			lessAll: './src/css/**/*.less',
			html: ['./src/html/**/*.html', '!./src/html/_*/**.html'],
			htmlAll: './src/html/**/*',
			php: './src/**/*.php'
		},
		tmp: {
			dir: './tmp',
			css: './tmp/css',
			img: './tmp/img',
			html: './tmp/html',
			sprite: './tmp/sprite'
		},
		dist: {
			dir: './dist',
			css: './dist/css',
			img: './dist/img',
			html: './dist/html',
			sprite: './dist/sprite'
		}
	},
	"lazyDir": ["../slice"], //gulp-lazyImageCSS 寻找目录(https://github.com/weixin/gulp-lazyimagecss)

	"supportWebp": false,

	"supportREM": false,

	"supportChanged": false,

	"reversion": false

}

module.exports = (function(protname) {
	return config;
})()