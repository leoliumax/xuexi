//var ua = navigator.userAgent.toLowerCase();
//var isAndroid = ua.indexOf("Android") > -1;
//var isIOS = ua.indexOf("iPhone") > -1 || ua.indexOf("iPad") > -1 || ua.indexOf("iPod") > -1;
//var isQQ = / qq\/([\d\.]+)/.test(ua);
//var isUC = / UCBrowser\/([\d\.]+)/.test(ua);

var browser = (function() {
	function matchVer(str) {
		var ua = navigator.userAgent.toLowerCase();
		var exp = new RegExp(str.toLowerCase() + "\\/([\\d.]+)");
		return ua.match(exp);
	}
	var brow = {
		ua: navigator.userAgent,
		type: function() {
			var u = navigator.userAgent;
			return {
				webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
				firefox: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
				mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
				ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
				winnt: u.indexOf("Windows") > -1,
				android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器
				iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
				iPad: u.indexOf('iPad') > -1, //是否iPad
				//webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
				weixin: u.indexOf('MicroMessenger') > -1, //是否微信 （2015-01-22新增）
				qq: u.indexOf('QQBrowser') > -1 //是否QQ （2015-01-22新增）
			};
		}(),
		versions: (function() {
			var u = navigator.userAgent;
			return {
				ie: u.indexOf('Trident') > -1 ? (u.indexOf('MSIE') > -1 ? u.match(/MSIE\s([\d.])+/) : u.match(/rev\:[\d.]+/)) : null,
				qq: matchVer("MQQBrowser") || matchVer("QQBrowser"),
				baidu: matchVer("baiduboxapp") || matchVer("BIDUBrowser"),
				winnt: u.match(/Windows\sNT\s[\d.]+/) || u.match(/Windows\sPhone\s[\d.]+/),
				chrome: matchVer("Chrome"),
				safari: matchVer("Safari"),
				weixin: matchVer("MicroMessenger"),
				firefox: matchVer("Firefox"),
				opera: matchVer("Opera"),
				ucbrowser: matchVer("UCBrowser") || u.match(/UCWEB[\d.]+/)
			}
		})(),
		language: (navigator.browserLanguage || navigator.language).toLowerCase()
	};
	return brow;
})();

//var isQQ = browser.versions.qq[0],
//	isIOS = browser.type.ios;
var visibility = (function() {
	var props = ["hidden", 'webkitHidden', 'mozHidden', 'msHidden', 'oHidden'];
	var prop = null;
	for (var i = 0; i < props.length; i++) {
		var p = props[i];
		if (p in document && typeof document[p] == 'boolean') {
			prop = p;
			break;
		}
	}
	return {
		support: !!prop,
		hiddenProp: prop,
		isHidden: function() {
			return document[prop];
		}
	};
})();

function matchVer(str) {
	var ua = navigator.userAgent;
	var exp = new RegExp(str + "\\/([\\d.]+)");
	return ua.match(exp);
}

function runApp(runUrl, failed) {
	var id = "__jump_id",
		e = document.getElementById(id),
		isQQ = browser.versions.qq[0],
		isIOS = browser.type.ios;
	if (!e) {
		e = document.createElement(isIOS && isQQ ? "a" : "iframe");
		e.setAttribute("id", id);
		e.style.display = "none";
		document.body.appendChild(e);
	}
	if (typeof failed == 'function') {
		var called = false;
		var start = Date.now();
		var callback = function() {
			if (!called && (called = true)) {
				var now = Date.now();
				if (now - start < 2000 && (!visibility.support || !visibility.isHidden())) {
					failed();
				}
				e.onload = null;
				callback = null;
			}
		}
		if (isIOS || visibility.support) {
			setTimeout(callback, 1500);
		} else {
			e.onload = callback;
		}
	}
	if (isIOS && isQQ) {
		e.href = runUrl;
		e.click();
	} else {
		e.src = runUrl;
	}
}

function installApp(installUrl) {
	var isQQ = browser.versions.qq[0],
		var isIOS = browser.type.ios;
	if (isQQ && isIOS) {
		runApp(installUrl);
	} else {
		window.open(installUrl, "_self");
	}
}
var docCookies = {
	getItem: function(sKey) {
		if (!sKey) {
			return null;
		}
		return decodeURIComponent(document.cookie.replace(new RegExp(
			"(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g,
				"\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
	},
	setItem: function(sKey, sValue, vEnd, sPath, sDomain, bSecure) {
		if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) {
			return false;
		}
		var sExpires = "";
		if (vEnd) {
			switch (vEnd.constructor) {
				case Number:
					sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
					break;
				case String:
					sExpires = "; expires=" + vEnd;
					break;
				case Date:
					sExpires = "; expires=" + vEnd.toUTCString();
					break;
			}
		}
		document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
		return true;
	},
	removeItem: function(sKey, sPath, sDomain) {
		if (!this.hasItem(sKey)) {
			return false;
		}
		document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
		return true;
	},
	hasItem: function(sKey) {
		if (!sKey) {
			return false;
		}
		return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
	},
	keys: function() {
		var aKeys = document.cookie.replace(
				/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "")
			.split(/\s*(?:\=[^;]*)?;\s*/);
		for (var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) {
			aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]);
		}
		return aKeys;
	}
};

function installVR(url) {
	if (!url) {
		return;
	}
	//大炮台---
	//		if(typeof NcyAppApi!='undefined' && NcyAppApi.openNcyApp){
	//			NcyAppApi.openNcyApp(url,"com.naocy.vrlauncher");
	//			//NcyAppApi.openNcyApp(url,"ncy.app.vrstore");
	//			return false;
	//		}
	if (typeof runApp == 'function') {
		runApp("ncy://home", function() {
			installApp(url);
		});
		return false;
	}
}