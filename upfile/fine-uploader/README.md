[![Fine Uploader](http://fineuploader.com/img/FineUploader_logo.png)](http://fineuploader.com/)

[![Build Status](https://travis-ci.org/FineUploader/fine-uploader.svg?branch=master)](https://travis-ci.org/FineUploader/fine-uploader)
[![npm](https://img.shields.io/npm/v/fine-uploader.svg)](https://www.npmjs.com/package/fine-uploader)
[![license](https://img.shields.io/badge/license-MIT-brightgreen.svg)](LICENSE)
[![Freenode](https://img.shields.io/badge/chat-on%20freenode-brightgreen.svg)](irc://chat.freenode.net/#fineuploader)
[![Stackoverflow](https://img.shields.io/stackexchange/stackoverflow/t/fine-uploader.svg)](http://stackoverflow.com/questions/tagged/fine-uploader)

[**Documentation**](http://docs.fineuploader.com) |
[**Examples**](http://fineuploader.com/demos) |
[**Support**](http://fineuploader.com/support.html) |
[**Blog**](http://blog.fineuploader.com/) |
[**Changelog**](http://blog.fineuploader.com/category/changelog/)

---

Fine Uploader is:

- Cross-browser
- Dependency-free
- 100% JavaScript
- 100% Free Open Source Software

FineUploader is also simple to use. In the simplest case, you only need to include one JavaScript file.
There are absolutely no other required external dependencies. For more information, please see the [**documentation**](http://docs.fineuploader.com).


## Contributing

Fine Uploader and all other projects in this organization thrive thanks to donations by Widen (the project's sponsor)
and support by developers in the community. If you'd like to help, the best way to do so is open up a pull request
that addresses one of the open feature requests or bugs. In order to get started developing Fine Uploader, follow these
steps to get the project up and running on your local development machine:

1. Download the project repository: `git clone https://github.com/FineUploader/fine-uploader.git`.
2. Install all project development dependencies: `npm install`.
3. To run the tests & linter: `$(npm bin)/grunt dev test:firefox` (you'll need Firefox installed locally).
4. Please follow the [Angular.js commit guidelines][angular-commit].
5. Please follow the [Git Flow][git-flow] branching strategy.


*Fine Uploader is a code library sponsored by [Widen Enterprises, Inc.](http://www.widen.com/)*

[angular-commit]: https://github.com/angular/angular.js/blob/master/CONTRIBUTING.md#commit
[git-flow]: http://nvie.com/posts/a-successful-git-branching-model/