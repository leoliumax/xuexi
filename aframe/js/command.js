var Control = {
	SetContainerSize: function SetContainerSize(id) {
		var el = document.getElementById(id);
		el.style.width = document.documentElement.clientWidth + "px";
		el.style.height = document.documentElement.clientHeight + "px";
	},
	weixin: function() {
		if (browser.type.weixin) {
			var el = document.getElementById("contentview");
			el.style['margin-top'] = "-50px";
		}
	},
	browser: function() {
		if (browser.type.weixin || browser.type.qq || browser.type.baidu) {
			var el = document.getElementById("contentview");
			el.style['margin-top'] = "-50px";
		}
	},
	testToogle: true
}

var viewProt = {
	width: document.documentElement.clientWidth,
	height: document.documentElement.clientHeight,
	screenwidth: window.screen.width,
	screenheight: window.screen.height,
	PixelRatio: devicePixelRatio,
	RaWidth: window.screen.width * devicePixelRatio,
	RaHeight: window.screen.height * devicePixelRatio
}

var browser = {
	ua: navigator.userAgent,
	type: function() {
		var u = navigator.userAgent;
		return {
			ie: u.indexOf('Trident') > -1, //IE内核
			opera: u.indexOf('Presto') > -1, //opera内核
			webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
			firefox: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
			mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
			ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
			winnt: u.indexOf("Windows") > -1,
			android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或者uc浏览器
			iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
			iPad: u.indexOf('iPad') > -1, //是否iPad
			//webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
			weixin: u.indexOf('MicroMessenger') > -1, //是否微信 （2015-01-22新增）
			qq: u.indexOf('QQBrowser') > -1 || u.indexOf('MQQBrowser') > -1, //是否QQ （2015-01-22新增）
			baidu: u.indexOf('baiduboxapp') > -1 || u.indexOf('baidubrowser') > -1 //是否QQ （2015-01-22新增）
		};
	}(),
	versions: (function() {
		function matchVer(str) {
			var ua = navigator.userAgent.toLowerCase();
			var exp = new RegExp(str.toLowerCase() + "\\/([\\d.]+)");
			return ua.match(exp);
		}
		var u = navigator.userAgent;
		return {
			ie: u.indexOf('Trident') > -1 ? (u.indexOf('MSIE') > -1 ? u.match(/MSIE\s([\d.])+/) : u.match(/rev\:[\d.]+/)) : null,
			qq: matchVer("MQQBrowser") || matchVer("QQBrowser"),
			baidu: matchVer("baiduboxapp") || matchVer("baidubrowser"),
			winnt: u.match(/Windows\sNT\s[\d.]+/) || u.match(/Windows\sPhone\s[\d.]+/),
			chrome: matchVer("Chrome"),
			safari: matchVer("Safari"),
			weixin: matchVer("MicroMessenger"),
			firefox: matchVer("Firefox"),
			opera: matchVer("Opera"),
			ucbrowser: matchVer("UCBrowser") || u.match(/UCWEB[\d.]+/)
		}
	})(),
	language: (navigator.browserLanguage || navigator.language).toLowerCase()
};

var fn = (function() {
	var fontSizeScale = 50 / 1080;

	function fontsizeFit() {
		var size = document.documentElement.clientWidth * fontSizeScale;
		var bodyel = document.documentElement;
		bodyel.style.fontSize = size + 'px';
	}
	var fn = function() {
		function FormCheck(el, clear) {
			var retu = false,
				type = {
					phone: function(value) {
						return /^1[3|4|5|6|7|8][0-9]\d{8}$/.test(value);
					}
				};

			function isCheck(values, el) {
				var elst = $(el).hasClass("inputerr");
				var elnext = $(el).next();
				var elerrtip = elnext.children(".tiptext");
				var hastip = function() {
						//判断悬浮文字是否存在
						return $(el).next().hasClass('inputerrtip');
					},
					removetip = function() {
						if (hastip()) {
							$('span.tiptext').unwrap();
							$('span.tiptext').remove();
						}
					},
					hidetip = function() {
						elerrtip.toggleClass("fadeOut");
					},
					errtip = function(el, msg) {
						if (hastip()) {
							elerrtip.toggleClass("fadeOut");
							return;
						} else {
							var tip = $('<div class="inputerrtip"></div>'),
								msgspan = $('<span class="tiptext"></span>').text(msg);
							$(el).after(tip);
							tip.append(msgspan);
							$(el).addClass('inputerr');
							$(tip).on('click', function() {
								$(this).children(".tiptext").toggleClass("fadeOut");
								$(this).hide();
								$(this).hasClass("inputerr") && $(this).removeClass("inputerr");
							});
						}
					};
				if (!values) {
					if ($(this).attr("placeholder")) {
						errtip(el, $(this).attr("placeholder"));
					} else {
						errtip(el, "此项必填");
					}
					return false
				};
				if ($(el).hasClass("phone") || $(el).attr("id").indexOf('phone') > -1) {
					if (!type.phone(values)) {
						removetip(el);
						errtip(el, '电话号码格式不正确');
						return false;
					};
				}
				if (el.type === 'password') {
					if (el.value.length < 6) {
						removetip(el);
						errtip(el, '密码长度不能小于6位');
						return false;
					}
					var els = $('input[type = password]');
					if (els[1].value && (els[0].value !== els[1].value)) {
						this.focus();
						!els[1].value && els[1].focus();
						$(els[1]).addClass('inputerr');
						removetip(el);
						errtip(el, '两次密码输入不一致，请重填');
						return false;
					}
				}
				return true;
			}
			//下面开始检查
			if (el[0]) {
				$(el).each(function() {
					if (!isCheck(this.value, this)) {
						$(this).focus();
						//					myScroll.scrollToElement(this, 1200, null, -50);
						return retu = false;
					} else {
						$(this).hasClass("inputerr") && $(this).removeClass("inputerr");
						retu = true;
					}
				})
				return retu;
			} else {
				return true;
			}
		}

		function toggInput(bl) {
			var footBut = $(".one-button,.two-button,.two-h-button")[bl ? "hide" : "show"]();
		}

		function loadingOk(el) {
			$(el || "#loading-lay").hide();
		}

		//安卓JS API调用
		function AndroidShare(url, title, content, iconUrl) {
			WebJs.share(url, title, content, iconUrl);
		}
		//			去登陆页
		function AndroidLogin(url) {
			WebJs.login(url)
		}

		function AndroidLaunch() {
			WebJs.launch()
		}

		function AndroidcloseWeb() {
			WebJs.closeWeb()
		}
		this.AndroidLaunch = AndroidLaunch;
		this.AndroidLogin = AndroidLogin;
		this.AndroidShare = AndroidShare;
		this.FormCheck = FormCheck;
		this.AndroidcloseWeb = AndroidcloseWeb;
		this.toggInput = toggInput;
		this.loadingOk = loadingOk;
		this.fontsizeFit = fontsizeFit;
	};
	var resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize';
	window.addEventListener(resizeEvt, fn.fontsizeFit, false);
	document.addEventListener('DOMContentLoaded', fn.fontsizeFit, false);
	return new fn();
})();


window.addEventListener('resize', function(e) {
	var lt = viewProt.height - document.documentElement.clientHeight;
	if (document.documentElement.clientWidth === viewProt.screenwidth || document.documentElement.clientHeight === viewProt.width) {
		fn.fontsizeFit()
	}
	//	fn.toggInput(lt > 10 ? true : false);
});

function Psd2Box() {

}

function checkForm() {
	var forms = $("form");
	var elarr = $("input[required]");
	if (elarr[0]) {
		forms.on('submit', function(e) {
			if (!fn.FormCheck($("input[required]"))) {
				e.preventDefault();
			}
		})
	}
}

$(document).ready(function() {
	checkForm(); //表单检查绑定;
	fn.loadingOk(); //隐藏loading层
	fn.fontsizeFit(); //rem适配
})